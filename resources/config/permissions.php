<?php

return [
    'company' => [
        'read',
        'write',
        'delete',
    ],
    'wizard' => [
        'read',
        'write',
        'delete',
    ],
];
