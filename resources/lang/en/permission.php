<?php

return [
    'company' => [
        'name'   => 'Company',
        'option' => [
            'read'   => 'Can read company?',
            'write'  => 'Can create/edit company?',
            'delete' => 'Can delete company?',
        ],
    ],
    'wizard' => [
        'name'   => 'Wizard',
        'option' => [
            'read'   => 'Can read wizard?',
            'write'  => 'Can create/edit wizard?',
            'delete' => 'Can delete wizard?',
        ],
    ],
];
