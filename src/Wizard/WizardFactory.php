<?php namespace Visiosoft\WizardModule\Wizard;

use Illuminate\Database\Eloquent\Factories\Factory;

class WizardFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var WizardModel
     */
    protected $model = WizardModel::class;


    /**
     * @return array
     * @throws \Exception
     */
    public function definition()
    {
        return [];
    }
}
