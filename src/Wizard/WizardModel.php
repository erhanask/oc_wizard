<?php namespace Visiosoft\WizardModule\Wizard;

use Visiosoft\WizardModule\Wizard\Contract\WizardInterface;
use Anomaly\Streams\Platform\Model\Wizard\WizardWizardEntryModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class WizardModel extends WizardWizardEntryModel implements WizardInterface
{
    use HasFactory;

    /**
     * @return WizardFactory
     */
    protected static function newFactory()
    {
        return WizardFactory::new();
    }
}
