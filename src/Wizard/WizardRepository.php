<?php namespace Visiosoft\WizardModule\Wizard;

use Visiosoft\WizardModule\Wizard\Contract\WizardRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class WizardRepository extends EntryRepository implements WizardRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var WizardModel
     */
    protected $model;

    /**
     * Create a new WizardRepository instance.
     *
     * @param WizardModel $model
     */
    public function __construct(WizardModel $model)
    {
        $this->model = $model;
    }
}
