<?php namespace Visiosoft\WizardModule\Wizard\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface WizardRepositoryInterface extends EntryRepositoryInterface
{

}
