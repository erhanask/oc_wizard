<?php

namespace Visiosoft\WizardModule\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\URL;

class LoginCheck
{

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $segment = "";

        //Getting Uri's first 5 characters. 
        if (Request::route()->uri() != "" && Request::route()->uri() != "/") {
            for ($i = 0; $i < 5; $i++) {
                $segment .= Request::route()->uri()[$i];
            }
        }

        //If first 5 characters equals admin , middleware works.
        if ($segment === 'admin' && Auth::user()) {

            if (Auth::user()->login_check == 0) {
                return redirect('/wizard');
            }

        }


        return $next($request);
    
    
    }
}
