<?php

namespace Visiosoft\WizardModule\Http\Controller;

use Anomaly\SettingsModule\Setting\SettingModel;
use Anomaly\Streams\Platform\Http\Controller\BaseController;
use Anomaly\UsersModule\User\UserModel;
use Illuminate\Http\Request;

class WizardController extends BaseController
{


    public function index()
    {

        $user_info = (new UserModel)->where('id', auth()->user()->id)->first();

        if ($user_info->login_check == 1) {
            return redirect('admin');
        }


        return view('visiosoft.module.wizard::wizard.index');
    }

    public function update(Request $request)
    {
        
        $user_info = (new UserModel)->where('id', auth()->user()->id)->first();
        $site_name = (new SettingModel)->where('key', 'streams::name')->first();
        $site_sender = (new SettingModel)->where('key', 'streams::sender')->first();
        $site_description = (new SettingModel)->where('key', 'streams::description')->first();
        $site_lang = (new SettingModel)->where('key', 'streams::default_locale')->first();
        $site_domain = (new SettingModel)->where('key', 'streams::domain')->first();

        $user_info->update([
            'login_check' => 1,
        ]);

        $site_name->update([
            'value' => $request->site_name
        ]);

        $site_sender->update([
            'value' => $request->site_name
        ]);

        $site_description->update([
            'value' => $request->site_description
        ]);

        $site_lang->update([
            'value' => $request->site_lang
        ]);

        $site_domain->update([
            'value' => $request->site_domain
        ]);

        return redirect('admin');
    }
}
